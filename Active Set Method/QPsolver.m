function [x, lambda ] = QPsolver(Q,A,c,b)
% pc.m resuelve el problema
%  Min (1/2)*x'*Q*x + c'*x
%  S.a. A*x=b

% IN
% Q.- es sim�trica definida positiva de orden n
% A.- es mxn con m<n y rango(A) =m
% c.- es un vector de orden n
% b.- es un vector de orden m

% Out
% x.- vector de longitud n, m�nimo del problema de programaci�n cuadr�tica
% lambda.- vector de longitud m, multiplicador de Lagrange asociado al problema
%------------------------------------------------------------------------------
%Programadores:
%N�stor S�nchez 117349
%Joaqu�n Flores 120227
%--------------------------------------------------------------

[m,n]=size(A);
K=[Q  A'; A zeros(m)];
ld= [-c;b];

w=K\ld;
x=w(1:n);
lambda=w(n+1:m+n);

end