function [x,lambdaeq,mu] = ActiveSetMethod(Q,A,F,c,b,d)
% M�todo de conjunto activo para aproximar el m�nimo de la funci�n
%    min   (1/2) x'*Q*x + c'*x
%    s. a.      A*x  = b
%               F*x >= d
%este programa esta inspirado en el pseudoc�digo del algoritmo del conjunto
%activo que se encuentra en el libro Numerical Optimization de Nocedal,
%pag. 460

%N�stor S�nchez 117349
%Joaqu�n Flores 120227
%--------------------------------------------------------------------------
%IN
% Q.- matriz s.p.d nxn.
% A.- matriz mxn con m<n rango(A) = m.
% F.- matriz pxn.
% c.- vector nx1.
% b.- vector mx1.
% d.- vector px1.
%
%Out
% x.- aproximaci�n al m�nimo local, vector de dimensi�n nx1
% fx.- valor de la funci�n objetivo en x
%--------------------------------------------------------------------------
% Funciones propias utilizadas:
% factibleQ.m .- regresa un punto factible en caso que exista
%             y una bandera indicando si se encontr� o no.
% pc.m .- regresa la soluci�n del problema cuadr�tico con �nicamente
%         la restricci�n de igualdad.
%--------------------------------------------------------------------------
% Constantes:
% tol .- tolerancia para el 0 computacional..
% maxiter .- n�mero m�ximo de iteraciones permitidas.
%--------------------------------------------------------------------------
[bandera,x]=findFeasiblePoint(A,F,b,d);
if bandera==1 % si el conjunto factible es vacio, se indica y termina el 
              % algoritmo.
    fprintf('El conjunto factible es vac�o\n')
    fx=[];
else% si no es vacio, checamos si la solucion esta en el abierto.
    x_abierto=QPsolver(Q,A,c,b);
    if sum(F*x_abierto<d)>0 % Si la solucion no esta en el abierto, 
                            % comenzamos el metodo del conjunto activo.
    %----------------------------------------------------------------------    
    % valores constantes   
        tol=1e-06;
        maxiter = 50;
    %----------------------------------------------------------------------    
    % valores iniciales
        flag=0; % indicador que se enciende si se encuentra la solucion del 
                % problema.
        iter = 0;
    %----------------------------------------------------------------------    
        % comenzamos con un punto factible.
        W=find(abs(F*x-d)<tol); % indices de las restricciones activas.
        
    % parte iterativa.     
          while (  iter<maxiter && flag==0)

              % construyo la matriz de restricciones activas.
              M=A;
              b_gorro=b;
              for i=1:length(W)
                  M=[M; F(W(i),:)];
                  b_gorro=[b_gorro; d(W(i))];
              end
              % encuentro la direccion optima que satisfaga las 
              % restricciones activas.
              [x_estrella,lambda]=QPsolver(Q,M,c,b_gorro);
              p=x_estrella-x;
              if (norm(p)<tol) %si p~=0.
                  lambda=-lambda;
                  lambda_ineq=lambda(size(A,1)+1:length(lambda)); 
                  % tomo los lagrangianos correspondientes a las 
                  % desigualdades activas.
                  if min(lambda_ineq)>0-tol % si todos sus lagrangianos son 
                                            % no negativos, terminamos.
                      flag=1; % se encontro la solucion, as� que se acaba 
                              % el programa.
                  else % si no, encuentro la restriccion de desigualdad 
                       % que tenga el menor lagrangiano.
                      j=find(lambda_ineq==min(lambda_ineq),1,'first');
                      W(j)=[]; % quito esa restriccion.
                      length(W);
                  end
              else % si p!=0, encuentro el tama�o de paso tal que x+p siga
                   % siendo factible.
                  v=zeros(2,size(F,1));  
                  for i=1:size(F,1) % calculo el paso permitido por las 
                                    % restricciones de desigualdad que no 
                                    % est�n activas.
                      if (F(i,:)*p<0 && sum(W==i)==0) 
                        v(1,i)=(d(i)-F(i,:)*x)/(F(i,:)*p); % primera fila
                                    % de v: restricciones al tama�o de
                                    % paso.
                        v(2,i)=i; %segunda fila: �ndice de la restricci�n.
                      else
                        v(1,i)=2;
                        v(2,i)=i;
                      end
                  end
                  v=[v [1 0]']; % se a�ade un 1, por si ninguna restricci�n
                                % es de bloqueo.
                  alfa=min(v(1,:));
                  x=x+alfa*p;
                  if alfa<1 % si hay una restriccion de bloqueo,
                            % se a�ade al working set.
                      h=v(2,find(v(1,:)==min(v(1,:)),1,'first'));%se toma 
                      % la primera restricci�n de bloqueo que se encuentre.
                      W=[W h]; % se a�ade el nuevo �ndice a W.
                  end
              end
                  iter=iter+1;
                  u(iter)=(1/2)*x'*Q*x+c'*x;  
          end 
    %----------------------------------------------------------------------
%     %  % graficaci�n de la funci�n objetivo contra el n�mero de iteraci�n     
%     plot(1:length(u),u,'Color',[255/255 150/255 0],'LineWidth',1.5)
%     title('Desempe�o del Algoritmo')
%     xlabel('Iteraciones')
%     ylabel('Funci�n Objetivo')
    %----------------------------------------------------------------------
    end 
    
end
L=(size(A,1)+1):length(lambda);
mu=lambda(L);
lambdaeq=lambda(1:size(A,1));
end

