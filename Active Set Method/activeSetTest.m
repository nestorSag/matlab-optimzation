% prueba_Conj_Activo.m
% calcula el tiempo de maquina necesario para resolver el problema
%    min   (1/2) x'*Q*x - c'*x
%    s. a.      A*x  = b
%               F*x >= d
% al usar los dos m�todos de Conjunto Activo que programamos: 
% ConjuntoActivoQ.m , ConjuntoActivo.m y el m�todo utilizado por matlab
% quadprog.m
% al incrementar la dimensi�n n 
%---------------------------------------------------------------------
% Programan:
% 
% N�stor S�nchez 117349
% Joaqu�n Flores 120227
%---------------------------------------------------------------------
% Funciones utilizadas:
% randQPGenerator.m :
%               Genera problemas cuadr�ticos 'aleatorios' de la forma
%               Min  (1/2)*x'*G*x + c'*x
%               s. a.   A*x = b;            (1)
%                       F*x >= d
% ConjuntoActivo.m :
%               Aproxima el m�nimo del problema (1) utilizando el m�todo
%               del conjunto activo.
% ConjuntoActivoQ.m :
%               Aproxima el m�nimo del problema (1) utilizando el m�todo
%               del conjunto activo. 
% quadprog.m:
%               Aproxima el m�nimo del problema (1) utilizando por default
%               el m�todo interior-point-convex.
%---------------------------------------------------------------------
% Constantes
% tau.- n�mero real positivo menor a 1.
%---------------------------------------------------------------------
tau=.85;
bolsa=[];  % guarda los tiempos del m�todo ConjuntoActivo.m
bolsa2=[]; % guarda los tiempos del m�todo quadprog.m

t_prom_Ca1=0; % para calcular el promedio de tiempo de cada m�todo.
t_prom_Quad=0;

% parte iterativa.
for n=30:20:600
    m=floor(n/3);
    r = floor(3*n/4); 
    [Q,A,F,c,b,d] = randQPGenerator(n,m,r,tau);
   
    
    t=cputime;
    [~,~] = ActiveSetMethod(Q,A,F,c,b,d);
    bolsa=[bolsa; cputime-t];
    t_prom_Ca1=t_prom_Ca1 +cputime-t;
   
    
    
    r=cputime;
    [~,~] = quadprog(Q,c,-F,-d,A,b);
    bolsa2=[bolsa2; cputime-r];
    t_prom_Quad=t_prom_Quad +cputime-r;
    
   
    
    
    
end
% graficaci�n del tiempo contra el valor de n.
r=length(bolsa); 
v=(1:r)'; 
plot(v,bolsa, 'rd',v,bolsa,'--b',v, bolsa2,'rd',v,bolsa2,'--g','Linewidth',3,'Linewidth',3)
title('Tiempo de m�quina de Conjunto Activo', 'Fontsize', 24)
legend('ConjuntoActivo','','quadprog','')

% c�lculo de los promedios.
t_prom_Ca1=t_prom_Ca1/r;
t_prom_Quad=t_prom_Quad/r;


fprintf('Tiempo Promedio\n ActiveSetMethod    QUADPROG\n %4.2f        %4.2f         %4.2f',t_prom_Ca1,t_prom_Quad)