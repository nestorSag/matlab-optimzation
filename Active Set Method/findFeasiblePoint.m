function [bandera, x ] = findFeasiblePoint(A,F,b,d)
% factibleQ.m encuentra un punto factible del problema de programaci�n
% cuadr�tica:
%   Min  (1/2)*x'*G*x + c'*x
%   s. a.   A*x = b;
%           F*x >= d
%--------------------------------------------------------------------------
%Programadores:
%N�stor S�nchez 117349
%Joaqu�n Flores 120227
%--------------------------------------------------------------------------
% IN
% A matriz mxn con rango = m
% F matriz rxn
% b vector mx1
% d vector rx1

% OUT
% x punto factible vector nx1
% bandera: 1 si el conjunto es vac�o
%          0 si el conjunto no es vac�o
%--------------------------------------------------------------------------
% Vamos a utilizar programaci�n lineal para encontrar el punto factible.
% Definimos el problema de programaci�n lineal:

% Min       zeros(n,1)'* x + ones(m,1)'* z
% s. a.    A*x = b
%          F*x + z >= d
%             z >= 0

% De tal manera que podamos utilizar la funci�n de Matlab linprog.m
%--------------------------------------------------------------------------
% Variables usadas:
% bandera.- n�mero natural 0 1
%           bandera = 0, se encontr� punto factible
%           bandera = 1, no existe un punto factible
%--------------------------------------------------------------------------
% Valores constantes:
[m,~]= size(A);
[r,n]= size(F);
%  x = zeros(n,1);
%--------------------------------------------------------------------------
% Valores iniciales:
bandera =0;
%--------------------------------------------------------------------------
%Creamos las matrices y el vector del lado derecho.
A_aux = [A zeros(m,r)];
c_aux = [zeros(n,1); ones(r,1)];
F_aux = [-F -eye(r)];

% Creamos las cotas superiores e inferiores de las variables
lb=[-inf*ones(n,1); zeros(r,1)];
up=inf*ones(n+r,1);

%Construimos un punto factible
% R =chol(A*A');
% u = trin(R',b);
% v = tris(R,u);
% x = A'*v;
% 
% z =zeros(r,1);
% for j =1:r
%     aux = F(j,:)*x - d(j);
%     if (aux < 0)
%         z(j) = -aux;
%     end
% %     aux = 0;
% end
% %tenemos nuestro primer punto factible
% w0 = [x;z];

    if  isempty(A) 
        
        if isempty(F)
        fprintf('No existen restricciones de igualdad ni de desigualdad\n')
        w = zeros(n+m,1);
        fw = 0;
        else
        [w, fw] = linprog(c_aux,F_aux,-d,[],[],lb,up);
        fprintf('Sin restricciones de igualdad\n')             
        end 
    
   else if isempty(F)  
        fprintf('Sin restricciones de desigualdad\n')
        [w,fw] = linprog(c_aux,[],[],A_aux,b,lb,up);      
        else
        [w,fw]= linprog(c_aux,F_aux,-d,A_aux,b,lb,up);      
            
           
        end   
    end
    

    if abs(fw) <=1.e-06
	x=w(1:n);
        fprintf('Punto factible encontrado\n')
    else
        fprintf('No existe punto factible\n')
       	x=[];
        bandera = 1;
    end    

end

