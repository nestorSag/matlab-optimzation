% Script file: Prueba_IntPoint.m
close all
%-----------------------------------------------------------
% problema aleatorio
tau = 0.85;
n = 50;
m = floor(n/3);
r = floor(3*n/4); 
[Q,A,F,c,b,d] = randQPGenerator(n,m,r,tau);  
xY = ActiveSetMethod(Q,A,F,c,b,d);


xM = quadprog(Q,c,-F,-d,A,b);