%prueba_intpts

tau=.85;
bolsa=[];  % guarda los tiempos del m�todo ConjuntoActivo.m
bolsa2=[]; % guarda los tiempos del m�todo quadprog.m

t_prom_Ca1=0; % para calcular el promedio de tiempo de cada m�todo.
t_prom_Quad=0;

% parte iterativa.
for n=30:20:600
    m=floor(n/3);
    r = floor(3*n/4); 
    [Q,A,F,c,b,d] = randQPGenerator(n,m,r,tau);
   
    
    t=cputime;
    [~,~] = interiorPointsMethod(Q,A,F,c,b,d);
    bolsa=[bolsa; cputime-t];
    t_prom_Ca1=t_prom_Ca1 +cputime-t;
   
    
    
    r=cputime;
    [~,~] = quadprog(Q,c,-F,-d,A,b);
    bolsa2=[bolsa2; cputime-r];
    t_prom_Quad=t_prom_Quad +cputime-r;
    
   
    
    
    
end
% graficaci�n del tiempo contra el valor de n.
r=length(bolsa); 
v=(1:r)'; 
plot(v,bolsa, 'rd',v,bolsa,'--b',v, bolsa2,'rd',v,bolsa2,'--g','Linewidth',3,'Linewidth',3)
title('Tiempo de m�quina de intpts', 'Fontsize', 24)
legend('intpts','','quadprog','')

% c�lculo de los promedios.
t_prom_Ca1=t_prom_Ca1/r;
t_prom_Quad=t_prom_Quad/r;


fprintf('Tiempo Promedio\n intpts    QUADPROG\n %4.2f        %4.2f         %4.2f',t_prom_Ca1,t_prom_Quad)