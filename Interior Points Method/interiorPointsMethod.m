function [x, lambda, mu, z] = interiorPointMethod(Q,A,F,c,b,d)
% NESTOR YURI SANCHEZ 117349
% Metodo de punto interior para el problema cuadr�tico
% Min   (1/2)* x' * Q * x + c�* x
% s.a.   A * x = b
%        F*x >= d

% 6 de marzo de 2014
% ITAM

% In
% Q.- matriz nxn sim�trica y positiva definida
% A.- matriz mxn con m <= n y rango(A) = m.
% F.- matriz pxn.
% c.- vector columna en R^n .
% b.- vector columna en R^m .
% d.- vector columna en R^p .
%
%Out
% x.- vector en R^n con la aproximaci�n del m�nimo local.
% lambda.- vector en R^m con la aproximaci�n al multiplicador
%          de Lagrange asociado a la restricci�n de igualdad.
% mu.- vector en R^p con la aproximaci�n al multiplicador
%          de Lagrange asociado a la restricci�n de desigualdad.
% z.- vector en R^p con la variable de holgura en la restricci�n
%     de desigualdad.

% Parametros iniciales
tol = 1e-08;
maxiter = 150;
cinter = 0;
%-----------------------------------------------------------
n = length(c);
m = length(b);
p = length(d);
%----------------------------------------------------------
x = findFeasiblePoint(A,F,b,d);
lambda = zeros(m,1);
mu = ones(p,1);
z = F*x - d + (0.5)*ones(p,1);
tau = (0.5)*(mu'*z)/p;
e=ones(p,1);
%-----------------------------------------------
% vectores para graficaci�n
cnpo=[]; comp =[];
% Norma de las condiciones necesarias de primer orden
H =[Q*x+A'*lambda-F'*mu+c; A*x-b;F*x-z-d;mu.*z];
norma = norm(H);
% disp('Iter      CNPO             tau ')
% disp('-----------------------------------------')
while(norma > tol & cinter < maxiter)
    % Resuelve el sistema lineal de Newton para la trayectoria
    % central con una iteraci�n. No se debe resolver el sistema completo.
    
    %Se crean las variables que se van a usar para encontrar Dx y Dlambda
    S=diag(z);
    S_inv=diag(1./z);
    U=diag(mu);
    U_inv=diag(1./mu);
    r_lambda=A*x-b;
    r_x=Q*x+A'*lambda-F'*mu+c;
    r_mu=-F*x+z+d;
    r_z=U*S*e-tau*e;
    G=Q+F'*(S_inv*U)*F;
    w=r_mu-U_inv*r_z;
    d_g=r_x-F'*(S_inv*U)*w;
    
    %se usa el programa de un laboratorio pasado (QP) para encontrar Dx y
    %Dlambda como las soluciones de un problema de programacion cuadratica
    [Dx,Dlambda]=QP(G,A,-d_g,-r_lambda);
    
    
    % Resolver el sistema aumentado.
    %-------------------------------------------------------
    % Se calculan los pasos:
    % Dx, Dlambda, Dmu, Dz
    Dmu=-(S_inv*U)*(F*Dx-w);
    Dz=-U_inv*(S*Dmu+r_z);
   %---------------------------------------------------------- 
    % Acorta el paso con alfa en (0, 1] tal que
      % mu + alfa* Dmu > 0    y  z + alfa*Dz >0
      alfa1 = stepSize(mu, Dmu);
      alfa2 = stepSize(z, Dz);
      % stepSize.m es una funci�n que deben crear en Matlab
      
     alfa = (0.995)*min( [alfa1, alfa2, 1]);
       
    
    %-----------------------------------------------------------
    % Nuevo punto
       x = x + alfa*Dx;
       lambda = lambda + alfa*Dlambda;
       mu = mu + alfa*Dmu;
       z  = z + alfa*Dz;
     %-------------------------------------------------------  
     % Nueva tau
        tau = (0.5)*(mu'*z)/p;
     %-------------------------------------------------------  
       %Condiciones necesarias de primer orden
       H=[Q*x+A'*lambda-F'*mu+c;A*x-b];
       H = [H;F*x-z-d;mu.*z];
       norma = norm(H);
       cinter = cinter + 1;
       cnpo =[cnpo norma];
       comp = [comp 2*tau];
%        disp(sprintf('%3.0f  %2.8f  %2.8f',cinter,norma,2*tau))
end

%    semilogy([1:cinter],cnpo,'r',[1:cinter],comp,'b')
%    title('Convergencia de puntos interiores')
%    legend('CNPO', 'Complementaridad')
        
