%IntPoint_test
tau = 0.85;
n = 50;
m = floor(n/3);
r = floor(3*n/4); 
[Q,A,F,c,b,d] = randQPGenerator(n,m,r,tau);

[x,lambda,mu,z]=interiorPointsMethod(Q,A,F,c,b,d);