function [ alfa ] = backtrackingInter( fname,x0,p,c1,g )
close all
%Se realiza bracketing y se encuentra el minimo de la interpolacion
%cuadratica de la funcion dentro del bracketing.

%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%IN.-
%fname.-       cadena de caracteres con el nombre de la funcion a
%x.-           (vector )punto inicial
%p.-           (vector )direccion de descendo
%c1.-          (escalar )parametro de condicion de Wolfe
%g.-           (vector )gradiente de la funcion en x

%OUT.-
%alfa          (escalar )valor donde la funcion fname(x0+alfa*p)

%Parametros
%maxiter.-     (escalar )numero maximo de iteraciones


close all
iter=0;
C=feval(fname,x0);
maxiter=100;
A=feval(fname,x0+p)-feval(fname,x0)-g'*p;
B=g'*p;
alfa = -B/(2*A); %minimo de la parabola (i.e. -A/2B)
while ((feval(fname,x0+alfa*p) > feval(fname,x0)+alfa*c1*g'*p) && (iter<maxiter)) %checar si se cumple armijo o alfa < 0.1
    p=alfa*p;
    alfa = -g'*p/(2*(feval(fname,x0+p)-g'*p-feval(fname,x0)));
    %if  alfa<.1;
    %    alfa=.1;
    %    exit=1;
    %end
end
B=g'*p;
A=feval(fname,x0+p)-feval(fname,x0)-g'*p;
t=linspace(0,1,50);
for i=1:50
    ft(i)=feval(fname,x0+t(i)*p);
    wolfe(i)=t(i)*B;
    poli(i)=A*t(i)^2+B*t(i)+C;
end
wolfe=feval(fname,x0)+c1*wolfe;
plot(t,ft)
hold on
plot(t,wolfe,'g')
hold on
plot(t,poli,'r')
legend('f(x)','Armijo','Interpolación')
end

