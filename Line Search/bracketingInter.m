function [ alfa ] = bracketingInter( fname,x0,p,c1,g )
%Se realiza bracketing y se encuentra el minimo de la interpolacion
%cuadratica de la funcion dentro del bracketing.
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%IN.-
%fname: nombre de la funcion
%x0:punto inicial (i.e. a=x*)
%p: direccion de decenso (i.e. b=x*+p)
%c1: parametro de la condicion de Armijo

%OUT.-
%xmin: valor donde la funcion cumple la condicion de Armijo
iter=0;
maxiter=50;
exit=0;
t = -g'*p/(2*(feval(fname,x0+p)-feval(fname,x0)-g'*p)); %minimo de la parabola (i.e. -A/2B)
while (feval(fname,x0+t*p) > feval(fname,x0)+t*c1*g'*p && iter<maxiter) && exit==0 %checar si se cumple armijo o alfa < 0.1
    p=t*p; %usar t*p como nuevo p y encontrar el minimo de la nueva parabola
    t = -g'*p/(2*(feval(fname,x0+p)-feval(fname,x0)-g'*p));
    iter=iter+1;
    %if  t<0.1
    %    t=0.1;
    %    exit=1;
    %end
end
alfa=t;
end

