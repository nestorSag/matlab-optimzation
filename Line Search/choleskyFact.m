function [L] = choleskyFact(A)
% Realiza la factorizacion de Cholesky de una matriz simetrica y definida
% positiva A y regresa una matriz triangular inferior L tal que A=L*L'
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%A.-                    matriz simmetrica

%Out
%L.-                    matriz triangular superior 

%Parametros
%tol.-                  valor para el cual un numero es lo suficientemente
%cercano a cero para considerarlo cero

tol=1.e-08;
v=eig(A);                          
vmin=min(v);                        %eighenvalor mas chico
[n,m] = size(A);
if vmin<tol
        A=A+(abs(vmin)+1)*eye(n); %si el eighen valor es mas chico q la toleracia le sumamos en valor absoluto un 1 para no tener perdida de precision
end
    
for k = 1:n-1
    A(k,k) = sqrt(A(k,k));
    A(k+1:n,k) = A(k+1:n,k)/A(k,k);
    A(k+1:n,k+1:n)=A(k+1:n,k+1:n)- A(k+1:n,k)*A(k+1:n,k)';
end
A(n,n) = sqrt(A(n,n));
L = tril(A)';
end
    


    
    
    
    