function [ alfa ] = goldenRatio( fname,x,p,c1,g )
% Aproximacion de mnimo local estricto para la fname
% en [a; b], por medio de la razon dorada.
% Salida: xmin aproximacion al mnimo local estricto,
% iter numero de iteraciones.
% Criterios de paro: b - a < 1e - 08 o iter > 100
close all
format long
r=(3-5^0.5)/2;
iter=1;
a=0;
b=1;
alfa=0.5;
maxiter=50;
while ((b-a)>=1e-08 && iter<maxiter)
    c=a+r*(b-a); % se secciona el intervalo
    d=a+(1-r)*(b-a); %se secciona el intervalo
    if feval(fname,x+c*p)<=feval(fname,x+d*p) %se deshecha una parte del intervalo
        b=d;
    else
        a=c;
    end
    iter=iter+1;
    alfa=(a+b)/2;
%     if (feval(fname,x+alfa*p) > feval(fname,x)+c1*alfa*g'*p) % condicion de Armijo (tambien sirve para verificar el supuesto de unimodularidad)
%         b=alfa;
%         a=0;
%         alfa=a/2;
%     end
end
end


