function  [gfx] = gradient(fname,x)
% Aproximacion del gradiente por diferencias centradas 
% de una funcion  de R^n a R.
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
% fname .- cadena con el nombre de la funcion.
% x .- vector columna de dimension n.
% Out

% gfx - vector columna de dimension n, es la aproximacion al
%       gradiente en x.

h = 1.e-08;   % tama�o de paso
n=length(x);
I=eye(n);

for k = 1:n
    gfx(k) = ( feval(fname,x+h*I(:,k)) -feval(fname,x-h*I(:,k)))/ (2*h);    % diferencias centradas
end
gfx=gfx';
end


