function [ y ] = griewangkFunc( x0 )
%calcula la funcion de griewangk en el punto x0
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x0.-           (vector ) valor donde se calculara la funcion Griewangk

%Out
%y.-            (escalar ) valor de la funcion Griewangk evaluada en x0

y=1+1/4000*x0'*x0;
prod=1;
for i=1:length(x0)
    prod=prod*cos(x0(i)/sqrt(i));
end
y=y-prod;

end

