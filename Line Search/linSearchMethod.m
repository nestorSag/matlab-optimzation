function [ x0,iter] = linSearchMethod(fname,x,decenso,backtraking )
%minimizacion de una funcion mediante busqueda de linea
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529


%IN
%fname.-                       cadena de caracteres con el nombre de la funcion a
%                               minimizar
%x0.-                          (vector) punto inicial donde se comienza a
%                                buscar el minimo
%decenso.-                       nombre del metodo para encontrar la direccion de decenso,
%                                puede ser 'maxDescent' (direccion de maximo decenso) o 'dirNewton'
%                                (direccion de newton)
%bracketing.-                    nombre del metodo para realizzar el bracketing:
%                                pueden ser 'BracketingInter' (interpolacion polinomial), 'RazonDorada'
%                                (minimizacion mediante el metodo de razon dorada) o 'backtracking'


%OUT.-
%x.-                             punto tal que el gradiente de fname en x es aproximadamente cero
%iter.-                          numero de iteraciones que hizo el algoritmo

%Parametros
%tol.-                           tolerancia para la norma del gradiente de f(x) es 1.e-08

%maxiter.-                       numero maximo de iteraciones externas maxiter = 50
%c1.-                            valor para la condicion de wolfe


maxiter=100;
tol=1.e-09;
c1=1.e-04;
iter=0;
x0=x;
g=gradient(fname,x0);
norma=norm(g);
while (norma > tol && iter<maxiter)
    p=feval(decenso,fname,x0,g);%direccion de decenso
    alfa=feval(backtraking,fname,x0,p,c1,g);
    x0=x0+alfa*p;
    iter=iter+1;
    if strcmp(fname,'rastrigin')==1 %este if usa una version mas precisa del gradiente numerico para rastrigin
        g=gradPreciso(fname,x0);
    else
    g=gradient(fname,x0);
    end
    norma=norm(g);
    %if(abs(alfa)<1.e-04)
    %    norma=0;
    %end
    
end

end

