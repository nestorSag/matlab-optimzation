function [ p ] = maxDescent( fname,x,g )
%calcula la direccion de maximo decenso de la funcion fname en x
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%fname.-       cadena de caracteres con el nombre de la funcion a
%minimizar
%x.-            (vector)punto en el cual se busca calcula la direccion de maximo
%descenso
%g.-            (vector)gradiente de la funcion en x

%Out
%p.-            (vector) direccion de maximo descensi


p=-g;       %calculamos la direccionde maximo descenso y la normalizamos

end

