function [x, lambda] = QPsolver(Q,A,c,b)
%min (1/2)*x'*Q*x+c'*x
%s. a. A*x=b
%Q sim. pos. de. de orden n
%A de mxn con m<n y rango(A)= m
%c vector de orden n
%b vector de orden m

k=[Q A'; A zeros(size(A,1))];
ld=[c;b];
sol=k\ld;
x=sol(1:size(Q,1));
lambda=sol(size(Q,1)+1:length(sol));
end