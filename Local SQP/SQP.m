function [ x,lambda,mu ] = SQP( fname,h_eq,h_uneq,metname,x0 )
%Algoritmo de programación cuadratica secuencial con igualdades y
%desigualdades
%resuelve el problema:
%min       f(x)
%s.a. h_eq(x)=0
%     h_uneq(x)>=0
%con f,h_eq,h_uneq, funciones dos veces continuamente diferenciables
%In.-
%fname.-nombre de la funcion objetivo
%h_eq.-nombre de la funcion de restricciones de igualdad
%h_uneq.-nombre de las funciones de restricciones de desigualdad
%metname.-nombre del metodo que se va a usar ('ConjuntoActivo' o 'intpnt')
%x0.-punto inicial
%Out.-
%x.-punto solución del problema
%lambda.-mult. de Lagrange en el punto solucion
%mu.-mult. de Lagrange de h_uneq en el punto solucion
iter=0;
maxiter=100;
tol=1e-06;
x=x0;
n=length(x);
heq=feval(h_eq,x);
m1=size(heq,1);
lambda=ones(m1,1);
Jeq=getJacobbian(h_eq,x);

if not(isempty(h_uneq))
    huneq=feval(h_uneq,x);
    m2=size(huneq,1);
    mu=ones(m2,1);
    Juneq=getJacobbian(h_uneq,x);
    gradlag=gradient(fname,x)+Jeq'*lambda-Juneq'*mu;
    CNPO=[gradlag;heq;huneq];
else
    gradlag=gradient(fname,x)+Jeq'*lambda;
    CNPO=[gradlag;heq];
end
disp('||CNPO||             f(x)         ||h_eq(x)||')
disp('---------------------------------------------')
while norm(CNPO)>tol && iter <maxiter
    %se construyen las matrices del subproblema
    Q=getHessian(fname,x)+getConstraintsHessian(h_eq,x,lambda);
    if not(isempty(h_uneq))
        Q=Q-getConstraintsHessian(h_uneq,x,mu);
        F=getJacobbian(h_uneq,x)';
        d=-feval(h_uneq,x);
    end
    delta=min(eig(Q));
    if delta < tol
        Q=Q+(1+abs(delta))*eye(n,n);
    end
    A=getJacobbian(h_eq,x);
    b=-feval(h_eq,x);
    c=gradient(fname,x);
    %subproblema, ya sea con conjunto activo o puntos interiores
    if not(isempty(h_uneq))
        if strcmp(metname,'activeSetMethod')
            [p,new_lambda,new_mu]=activeSetMethod(Q,A,F,c,b,d);
            x=x+p;
            lambda=new_lambda;
            mu=new_mu;
        end

        if strcmp(metname,'intpts')
            [p,new_lambda,new_mu]=interiorPointsMethod(Q,A,F,c,b,d);
            x=x+p;
            mu=new_mu;
            lambda=new_lambda;
        end
    else
        [p,new_lambda]=QPsolver(Q,A,-c,b);
        lambda=new_lambda;
    end
    %se construye el vector de CNPO's
    x=x+p;
    Jeq=getJacobbian(h_eq,x);
    if not(isempty(h_uneq))
        Juneq=getJacobbian(h_uneq,x);
        gradlag=gradient(fname,x)+Jeq'*lambda-Juneq'*mu;
        CNPO=[gradlag;heq;huneq];
    else
        gradlag=gradient(fname,x)+Jeq'*lambda;
        CNPO=[gradlag;feval(h_eq,x)];
    end
    iter=iter+1;
   disp(sprintf('%3.5f      %2.4f       %2.8f',norm(CNPO),feval(fname,x),norm(feval(h_eq,x))))
end
if not(isempty(h_uneq))
    mu=[];
end
end

