function [ x ] = findFeasiblePoint( A,F,b,d )
%NESTOR YURI SANCHEZ 117349
%esta funcion encuentra un punto factible del conjunto convexo Ax=b, Fx>=d
%por medio de programacion lineal


%IN.-
%A.- matriz de mxn
%F.-Matriz de rxr
%b.-vector columna de orden m
%d.-vector columnda de orden r



%OUT.-
%x.-vector columna de orden n, punto factible del conjunto

%se plantea el problema de PL

%min zeros(n,1)'*x+ones(m,1)'*z
%s.a.      Ax=b
%          Fx+z>=d
%          x,z>=0
%vector de la funcion objetivo
[m,n]=size(A);
[r,n]=size(F);
x=zeros(n,1);
z=zeros(r,1);
fc=[zeros(n,1); ones(r,1)];
Aiq=[-F -eye(r)];
Aeq=[A zeros(m,r)];
 for k = 1:n
    LB(k) = -inf;
    UB(k) = inf;
 end
 
 LB =[LB' ;  zeros(r,1)]';
 
 for k = (n+1):(n+r)
    UB(k) = inf;
 end
 
[w,fw]=linprog(fc,Aiq,-d,Aeq,b,LB,UB);

disp('valor de la funcion lineal')
fw
if (abs(fw)<=1e-06)
    x=w(1:n);
else
    disp('el conjunto es numericamente vacio')
    x=[];
end


end

