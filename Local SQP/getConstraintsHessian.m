function [ H ] = getConstraintsHessian( fname,a,lambda )
%calcula la hessiana de las restricciones
h=1e-06;
n=length(a);
m=size(feval(fname,a),1);
I=eye(n);
Hess=zeros(n,n);
for k=1:m
    for i=1:n
        for j=1:i
            s1=feval(fname,(a+h*I(:,i)+h*I(:,j)));
            s2=feval(fname,(a+h*I(:,i)));
            s3=feval(fname,(a+h*I(:,j)));
            s4=feval(fname,(a));
            H(i,j)=(s1(k)-s2(k)-s3(k)+s4(k))/h^2;
        end
    end
    for i=1:n
        for j=(i+1):n
            H(i,j)=H(j,i);
        end
    end
Hess=Hess+lambda(k)*H;
end

end

