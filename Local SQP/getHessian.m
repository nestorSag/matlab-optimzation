function [ H ] = getHessian(fname,a)
%calcula la matriz hessiana de la funcion fname.m en el vector a. La matriz
%H es de nxn simetrica y tiene la aproximacion a la matriz hessiana
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%fname.-       cadena de caracteres con el nombre de la funcion a
% calcular su Hessiana
%a.-           (vector )punto donde se calculara la Hessiana de fname

%Out
%H.-            (Matriz ) Hessiana de fname en a

h=1e-06;
n=length(a);
I=eye(n);
H=zeros(n,n);
for i=1:n
    for j=1:i
        H(i,j)=(feval(fname,(a+h*I(:,i)+h*I(:,j)))-feval(fname,(a+h*I(:,i)))-feval(fname,(a+h*I(:,j)))+feval(fname,(a)))/h^2;
    end
end
for i=1:n
    for j=i+1:n
        H(i,j)=H(j,i);
    end
end
end