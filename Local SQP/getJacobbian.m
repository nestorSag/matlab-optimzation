function [ J ] = getJacobbian( fname,x0 )
%Calcula la Jacobiana de la funcion fname en el punto x0
x=x0;
n=length(x);
I=eye(n);
m=size(feval(fname,x),1);
h=1e-06;
J=zeros(m,n);
for i=1:n
    fw=feval(fname,x+h*I(:,i));
    bw=feval(fname,x-h*I(:,i)); 
    J(:,i)= (fw-bw)/(2*h);
end

end

