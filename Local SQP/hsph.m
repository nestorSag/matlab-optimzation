function [ h ] = hsph( x )
%restricciones de la funci�n esfera
n=length(x)/3;
h=zeros(n,1);
for i=1:n
    h(i)=x(3*i-2)^2+x(3*i-1)^2+x(3*i)^2-1;
end

end

