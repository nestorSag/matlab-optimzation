n=15
x=[];
for i=1:n
    z=rand(3,1);
    z=z/norm(z);
    x=[x;z];
end
[x1,lambda]=SQP('fsph','hsph',[],[],x);
for i=1:n
    X(i)=x1(3*i-2);
    Y(i)=x1(3*i-1);
    Z(i)=x1(3*i);
end
sphere(40)
axis equal
hold on
scatter3(X,Y,Z,'*')
