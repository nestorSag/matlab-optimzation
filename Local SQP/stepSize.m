function [ alfa ] = stepSize( x,Dx )
%Esta funcion encuentra el recorte de paso para el algoritmo de puntos
%interiores
for i=1:length(x)
    if Dx(i)>=0
        alfa(i)=1;
    else
        alfa(i)=-x(i)/Dx(i);
    end
    

end

