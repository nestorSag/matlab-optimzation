function [ p ] = GMdir(g,Hess)
%Calcula la direccion de Gauss-Markov en el algoritmo LM
H=Hess;
tol=1e-06;
delta=min(eig(H));
if delta<tol
    H=H+(1+abs(delta))*eye(size(H,1));
end
L=chol(H,'lower');
y=solveLowerTriangular(L,-g);
p=solveUpperTriangular(L',y);


end

