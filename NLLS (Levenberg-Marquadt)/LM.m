function [beta,iter,f] =LM( fname,beta_ini,data )
%Nestor Sanchez 117349
%Fernando Payro 116529
%Algoritmo de Levenberg-Marquadt de m�nimos cuadrados no lineales
%In.-
%fname.- nombre de la funcion a ajustar
%beta.-vector de parametros iniciales
%data.- matriz de datos observados
%Out.-
%beta.-vector final de parametros para ajustar el modelo
%iter.- numero de iteraciones realizadas
%f.- suma de residuales al cuadrado en la soluci�n

%parametros iniciales
close all
c1=1e-04;
beta=beta_ini;
tol=1e-06;
maxiter=1000;
deltamin=1e-05;
deltamax=5;
delta=(deltamin+deltamax)/2;
g=LMGradient(fname,beta,data);
iter=0;
ploty=[];
while norm(g)>tol && iter<maxiter
    g=LMGradient(fname,beta,data);
    H=LMHessian(fname,beta,data);
    %minimizar el paraboloide en la region de confianza con Doblez
    pn=GMdir(g,H);
    pc=-(g'*g)/(g'*H*g)*g;
    p=Doblez2(pn,pc,delta);
    %vemos si se cumple la condicion de Wolfe
    while feval('squaredResiduals',fname,beta+p,data)>feval('squaredResiduals',fname,beta,data)+ c1*g'*p && delta>deltamin
        delta=max(delta/2,deltamin);
        p=Doblez2(pn,pc,delta);
    end
    %checamos la reduccion predicha y la reduccion actual
    if (feval('squaredResiduals',fname,beta+p,data)-feval('squaredResiduals',fname,beta,data))/(feval('paraboloideLM',fname,beta,data,H,g,p)-feval('paraboloideLM',fname,beta,data,H,g,zeros(length(g),1))) >=0.75
        delta=min(2*delta,deltamax);
    end
    beta=beta+p;
    iter=iter+1;
    
end
f=feval('squaredResiduals',fname,beta,data);
end

