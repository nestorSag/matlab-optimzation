function  [g] = LMGradient(fname,beta,data)
% Para el caso del algoritmo Levenberg-Marquadt, calcula el equivalente del 
% gradiente g al encontrar el punto de  Newton mediante p=-inv(H)*g
% es decir, calcula J'*r
%In.-
%fname.- nombre de la funcion
%beta.- vector actual de parametros
%data.-matriz de datos observados
%Out.-
%g.- g=-J'*res(fname,beta,data);
J=getJacobbian(fname,beta,data);
r=res(fname,beta,data);
g=J'*r;
end


