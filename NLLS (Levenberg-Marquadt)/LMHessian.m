function [ H ] = LMHessian(fname,beta,data)
%calcula el equivalente (en el algoritmo LM) a la matriz hessiana H al 
%momento de obtener la direccion de Newton mediante p=-inv(H)*g
%es decir, calcula J'*J

J=getJacobbian(fname,beta,data);
H=J'*J;
end