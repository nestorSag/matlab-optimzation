function [ J ] = getJacobbian(fname,beta,data )
%Calcula la jacobiana de un vector de residuales con respecto a sus n parametros
%invocando la funcion res con parametros fname,beta,data
%In.-
%fname.-nombre de la funcion
%beta.- vector de parametros en donde se va a derivar
%data.-matriz de datos 
%Out.-
%J.- matriz jacobiana
m=size(data,1);
n=length(beta);
I=eye(n);
J=zeros(m,n);
h=1e-06;

for j=1:n
    fs=feval('res',fname,beta+h*I(:,j),data);
    bs=feval('res',fname,beta-h*I(:,j),data);
    J(:,j)=(fs-bs)/h;
end


end

