function [ y ] = lineal( beta,x )
%funcion lineal. el intercepto se toma como beta(1) y de beta(2) en
%adelante son los coeficientes de las variables
%In.-
%beta.- parametros del hiperplano
%x.- punto en donde se va a evaluar la funcion
n=length(beta);
y=beta(1)+beta(2:n)'*x;

end

