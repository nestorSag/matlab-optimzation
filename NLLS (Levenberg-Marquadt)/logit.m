function [ y ] = logit( beta,x )
%Calcula el valor de la funcion logistica evaluada en un punto y con
%determinados parametros
%In.-
%beta.- parametros fijos
%x.-poblacion
y=beta(2)/(1+(beta(2)/beta(3)-1)*exp(-beta(1)*x));
end

