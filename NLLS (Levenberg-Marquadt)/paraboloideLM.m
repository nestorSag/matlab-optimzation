function [ y ] = paraboloideLM( fname,beta,data,H,g,p)
%calcula la aproximacion de newton de segundo orden de la funcion fname en
%el punto x con gradiente g y hessiana H en el punto x
y=0.5*p'*H*p+p'*g+feval('squaredResiduals',fname,beta,data);

end

