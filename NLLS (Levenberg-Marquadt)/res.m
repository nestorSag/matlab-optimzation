function [ r ] = res( fname,beta,data )
%Calculo de los residuales del problema en la iteracion actual
%In.-
%data.- matriz de datos de la forma [X y], donde y es la respuesta
%observada
%fname.- nombre de la funcion que se piensa ajustar a los datos
%beta.- vector de parametros actual
%Out.-
%r.-vector de residuales
[m,n]=size(data);
r=zeros(n,1);
for i=1:m
    r(i)=feval(fname,beta,data(i,1:(n-1))')-data(i,n);
end


end

