function [f] = squaredResiduals( fname,beta,data )
%calcula la suma de cuadrados de los residuales invocando la funcion res
%con los arguments fname,beta,data
%In.-
%fname.-nombre de la funcion que se quiere ajustar
%beta.-vector de los parametros actuales
%data.-matriz de datos observados de la forma [X y]
%Out.-
%f.-valor de la suma de cuadrados de los residuales
f=norm(res(fname,beta,data))^2;

end

