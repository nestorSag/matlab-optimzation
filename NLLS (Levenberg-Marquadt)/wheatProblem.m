%trigo.m
%prueba del algoritmo LM utilizando datos de produccion anual de trigo
%Nestor Sanchez 117349
%Fernando Payro 116529
%f(x*) es la suma de residuales al cuadrado en x*
beta0=[0.005;1;30];
fname='logit';
load agri_data
data=agri_data;
[beta,iter,fquad]=LM(fname,beta0,data);
norma=norm(LMGradient(fname,beta,data));
x=linspace(0,max(data(:,1)),1000);
for j=1:1000;
    f(j)=feval(fname,beta,x(j));
end
plot(x,f,'r')
for j=1:size(data,1)
    hold on
    plot(data(j,1),data(j,2),'*')
end
title('observados vs ajustados')

legend('Ajustados','valores observados')

xlabel('a�o')

ylabel('Producci�n de trigo')
str1=sprintf('Iteraciones=%d, f(x*)=%d',iter,fquad);
annotation('textbox',[.68 0.15 .22 .1],'String',str1)

disp(' ')
disp('Los parametros finales son: ')
disp(' ')


disp(['- r = ',num2str(beta(1))])
disp(['- k = ',num2str(beta(2))])
disp(['- P0 = ',num2str(beta(3))])
disp(['- iter = ',num2str(iter)])
disp(['- Norma del Gradiente = ',num2str(norma)])
