function [ T ] = coveringTree( n,E )
%Esta funci�n verifica que la gr�fica definida por E sea conexa
%y encuentra un �rbol generador
%input: n=numero de vertices, E=extremos de las aristas
%output: T=extremos de las aristas del �rbol generador
vT=[]; %conjunto de vertices que forman parte de un arbol (inicializado en vacio)
T=[]; %conjunto de aristas de el o los arboles
etq=[];
etq(:,1)=1:n;
etq(:,2)=zeros(n,1); %matriz cuyas columnas son vertices y etiquetas (inicializadas en cero)
ntree=0; %numero actual de arboles (inicializado en cero)
e=size(E,1);

%------------esta seccion se usar� mas adelante para graficar de forma conveniente la grafica y e arbol
A=zeros(n,n); 
for i=1:e
    A(E(i,1),E(i,2))=1;
    A(E(i,2),E(i,1))=1;
end
%-------------

while(size(T,1)<(n-1) && size(E,1)>0)
    if((ismember(E(1,1),vT)+ismember(E(1,2),vT)==0))
        T=[T; E(1,:)];
        etq(E(1,1),2)=ntree+1; %etiquetado
        etq(E(1,2),2)=ntree+1; %etiquetado
        ntree=ntree+1;
        vT=[vT; E(1,1); E(1,2)];
        E(1,:)=[];
    else
       if(ismember(E(1,1),vT)+ismember(E(1,2),vT)==1)
           T=[T; E(1,:)];
           if(ismember(E(1,1),vT)==0)
               etq(E(1,1),2)=etq(E(1,2),2);
           else
               etq(E(1,2),2)=etq(E(1,1),2);
           end
           vT=unique([vT; E(1,1); E(1,2)]);
           E(1,:)=[];
       else
           if(etq(E(1,1),2)==etq(E(1,2),2))
               E(1,:)=[];
           else
               T=[T; E(1,:)];
               new_lbl=min(etq(E(1,1),2),etq(E(1,2),2));
               old_lbl=max(etq(E(1,1),2),etq(E(1,2),2));
               idx=1:n;
               etq(idx(etq(:,2)==old_lbl),2)=new_lbl;
               E(1,:)=[];
           end
       end
    end
end
ntree=length(unique(etq(:,2)));

% a partir de aqui, el codigo es solamente para graficar
AT=zeros(n);
for i=1:size(T,1)
    AT(T(i,1),T(i,2))=1;
    AT(T(i,2),T(i,1))=1;
end
l=linspace(0,2*pi,n+1)';
x=cos(l);
y=sin(l);
coord=[x y];
if(ntree>1 || length(vT)<n)
    fprintf('La gr�fica no es conexa \n')
    subplot(1,2,1)
    gplot(A,coord,'-*');
    for i=1:n
    hold on
    str=num2str(i);
    text(coord(i,1),coord(i,2),str,'FontSize',15)
    end
    axis square
    title('Gr�fica Original')
    subplot(1,2,2)
    gplot(AT,coord,'-*')
    for i=1:n
    hold on
    str=num2str(i);
    text(coord(i,1),coord(i,2),str,'FontSize',15)
    end
    axis square
    title('bosque de arboles generadores')
else
    subplot(1,2,1)
    gplot(A,coord,'-*');
    for i=1:n
    hold on
    str=num2str(i);
    text(coord(i,1),coord(i,2),str,'FontSize',15)
    end
    axis square
    title('Gr�fica Original')
    subplot(1,2,2)
    gplot(AT,coord,'-*')
    for i=1:n
    hold on
    str=num2str(i);
    text(coord(i,1),coord(i,2),str,'FontSize',15)
    end
    axis square
    title('�rbol generador')
end
end
