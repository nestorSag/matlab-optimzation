%Algoritmo para encontrar un arbol de recubrimiento para gr�ficas conexas
%N�stor S�nchez 117349
%IDO



E=[1 2;2 3;3 4;4 5;5 3;6 4; 6 7; 7 1; 8 9; 9 10; 10 11; 11 12; 12 10; 12 9; 12 8];
n=12;

fprintf('Prueba con una gr�fica disconexa: \n')
T=coveringTree(n,E);
pause 
close all
fprintf('prueba con una gr�fica completa: \n')
E=[];
for i=1:9
    for j=(i+1):10
        E=[E; i j];
    end
end
n=10;
T=coveringTree(n,E);