# README #


### What is this repository for? ###

* This repository contains some of the numerical methods for constrained and unconstrained optimization problems that I programmed for my bachelor's numerical analysis courses using MATLAB.  

### How do I get set up? ###

To run any of the algorithms, simply download all the folder's files to your working directory. Some of the folder's files are not part of the algorithm, but functions and problems to be used as tests.


### Who do I talk to? ###
nestor.sag@gmail.com