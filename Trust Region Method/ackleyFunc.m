function [ y ] = ackleyFunc( x0 )
%calcula el valor  de la funcion de Ackley evaluada en x0
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x0.-           (vector ) valor donde se calculara la funcion Ackley

%Out
%y.-            (escalar ) valor de la funcion Ackley evaluada en x0

d=length(x0);
y=-20*exp(-0.2*sqrt(x0'*x0/d))+20+exp(1)-exp(sum(cos(2*pi*x0))/d);


end

