function [ y ] = braninFunc( x0 )
%calcula la funcion de Branin evaluada en x0
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x0.-           (vector ) valor donde se calculara la funcion Branin

%Out
%y.-            (escalar ) valor de la funcion Branin evaluada en x0

y=(x0(2)-5.1/(4*pi^2)*x0(1)^2+5/pi*x0(1)-6)^2+10*(1-1/(8*pi))*cos(x0(1))+10;


end

