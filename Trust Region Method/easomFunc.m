function [ y ] = easomFunc( x0 )
%calcula la funcion de Eason evaluada en x0
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x0.-           (vector ) valor donde se calculara la funcion Easom

%Out
%y.-            (escalar ) valor de la funcion Easom evaluada en x0

y=-cos(x0(1))*cos(x0(2))*exp(-((x0(1)-pi)^2+(x0(2)-pi)^2));


end

