function fx = griewangkFunc(x)
% Funci�n de Griewank (M�nimo f(0,0)=0):
fx=((x(1)^2+x(2)^2)/4000)-cos(x(1))*(cos(x(2)/sqrt(2)))+1;
end