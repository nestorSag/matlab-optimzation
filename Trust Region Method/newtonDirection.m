function [ p ] = newtonDirection fname, x,g )
%calcula la direccion de Newton de fname en x. Si la hessiana no es
%definida positiva, se encuentra una aproximacion definida positiva a la
%hessiana
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%fname.-        cadena de caracteres con el nombre de la funcion a
%minimizar
%x.-            (vector )punto donde se desea calcular la direccion de Newton
%g.-            (vector )gradiente de la funcion en x

%Out
%p.-            (vector )direccion de descenso de newton

%Parametros
%tol.-          (escalar)tolerancia para la cual un numero es
%suficientemente cercano a cero que lo consideramos cero


tol =1e-05;
mat=getHessian(fname,x);          %calculamos la hessiana de fname en x
sigma=min(eig(mat));            %calculamos su eighenvalor mas chico para 
if sigma < tol                  %verificar que la matriz hessiana sea positiva 
    mat=mat+(abs(sigma)+1)*eye(length(x));   %definida, pues si tiene un eighenvalor cero o menor que cero no lo es
end                             %en caso de no ser positiva definida la aproximamos 
    R=chol(mat);            %calculamos la factorizacion de cholesky de la matriz anterior para 
    p=R\(R'\-g); %resolver el sistema de equaciones que presenta la direccion de Newton
    %p=p/norm(p);


end

