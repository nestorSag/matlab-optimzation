function [ y ] = rastriginFunc( x0 )
%calcula la funcion de Rastrigin en el punto x0
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x0                 (vector )valor en el cual se busca calcular la funcion de
%Rastrigin

%Out
%y.-                (escalar )valor de la funcion Rastrigin evaluada en x0

y=10*length(x0);
for i=1:length(x0)
    y=y+x0(i)^2-10*cos(2*pi*x0(i));
end
end

