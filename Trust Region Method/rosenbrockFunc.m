function [ y ] = rosenbrockFunc( x )
%funcion de Rosenbrock
%Nombre: Nestor Sanchez
%Clave Unica: 117349
%Fernando Payro
%Clave Unica: 116529

%In
%x.-                (vector) punto en el que se busca evaluar la funcion de
%Rosenbrock

%Out
%y.-                (escalat) valor de la funcion de Rosenbrock evaluada en
%x0


y=(1-x(1))^2+100*(x(2)-x(1)^2)^2;

end

