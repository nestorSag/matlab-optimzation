function [ x,iter ] = trustRegionMethod( fname,x0 )
%algoritmo de region de confianza para minimizacion
%In.-
%fname: nombre de la funcion a minimizar
%x0: punto inicial
%Out.-
%x: punto final tal que ||graf(fname,x)||<tol
%iter: numero de iteraciones que tarda para llegar de x0 a x

%parametros iniciales
c1=1e-04;
tol=1e-08;
x=x0;
maxiter=100;
deltamin=1e-05;
deltamax=1;
delta=(deltamin+deltamax)/2;
g=getGradient(fname,x);
iter=0;
while norm(g)>tol && iter < maxiter
    g=getGradient(fname,x);
    H=getHessian(fname,x);
    %minimizar el paraboloide en la region de confianza con Doblez
    pn=newtonDirectionfname,x,g);
    pc=-(g'*g)/(g'*H*g)*g;
    p=Doblez2(pn,pc,delta);
    %vemos si se cumple la condicion de Wolfe
    while feval(fname,x+p)>feval(fname,x)+ c1*g'*p && delta>deltamin
        delta=max(delta/2,deltamin);
        p=Doblez2(pn,pc,delta);
    end
    %checamos la reduccion predicha y la reduccion actual
    if (feval(fname,x+p)-feval(fname,x))/(feval('paraboloide',fname,x,g,H,p)-feval('paraboloide',fname,x,g,H,zeros(length(x),1))) >=0.75
        delta=min(2*delta,deltamax);
    end
    x=x+p;
    iter=iter+1;
end
end

