function [ s ] = Principal( H, restin, k ,sep)
%Carlos Jacome
%Nestor Sanchez
%Esta funcion encuentra una coloracion de una grafica por el metodo de
%enumeracion implicita.
%IN.-
%H.- matriz que caracteriza el grafo; puede ser matriz de adyascencia o de
%cliques
%metoodo.- Especifica si A es matriz de adyascencia o de cliquez
%restin.- vector de restricciones del problema
%k.- "salones" disponibles
%sep.- separacion entre colores de vertices adyascentes
%Out.- 
%s.- vector solucion del problema; la entrada i especifica cual es el
%color del nodo i
%M.-matriz de resolucion
if max(max(H))>1 %crear la matriz de adyascencias si es necesario
    A=cliques(H);
else
   A=H;
end

%inicializar parametros
n=size(A,1);
lab=1:n;
restvec=zeros(n,1);
s=zeros(n,1);
k1=k;

%algoritmo
s=Coloracion(restin,A,lab,restvec,s,A, k1, sep);

%graficacion usando los eigenvectores 2 y 3 de la matriz laplaciana
xy=rand(n,2);
gplot(A,xy,'-*');
for i=1:n
    hold on
    str=num2str(s(i));
    text(xy(i,1),xy(i,2),str,'FontSize',18)
end

