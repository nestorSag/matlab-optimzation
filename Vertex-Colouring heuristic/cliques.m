function [ A ] = cliques( B )
%Esta matriz convierte la matriz de cliques en matriz de adyascencias
n=max(max(B));
A=zeros(n,n);
m=size(B,2);
for k=1:size(B,1)
    for i=1:m-1
        for j=(i+1):m
            if (B(k,i)>0 && B(k,j)>0)
                A(B(k,i),B(k,j))=1;
                A(B(k,j),B(k,i))=1;
            end
        end
    end
end


end

