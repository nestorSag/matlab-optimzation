function [s1 ] = coloring( restin,A,lab,restvec,s,Ain, k, sep)
%funcion que realiza la coloracion de vertices
%In.-
%A.- matriz de adyascencias del grafo (sin vertices coloreados)
%lab.- etiquetas de los vertices
%restvec.-restricciones causadas por los vecinos
%restin.-restricciones iniciales
%s.- vector de coloracion
%A.-Matriz de adyascencia original
%k.- "salones" disponibles
%sep.- separacion entre colores de vertices adyascentes
%Out.-
% vector de coloracion (el vertice i tiene color s(i))
deg=sum(A);
s1=s;
A1=A;
restvec1=restvec;
lab1=lab;
if sum(deg==0)>0 %siguiente vertice a colorear
    j=find(sum(A)==0,1,'first');
    i=lab(j);
else
    j=find(deg==max(deg),1,'first');
    i=lab(j);
end
r=restin(i);
while sum(restvec(i,:)==r)>0 || sum(s==r)>=k
    r=r+1;
end
s1(i)=r;
A1(j,:)=[];
A1(:,j)=[];
lab1(j)=[];
for m=1:sep
    restvec1=[restvec1 (r+m-1)*Ain(:,i)];
end
if not(isempty(lab1)) %llamada recursiva
    s1=coloring(restin,A1,lab1,restvec1,s1,Ain, k, sep);
end
    
end

