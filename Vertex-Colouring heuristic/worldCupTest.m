
%se modela la primera fase del mundial como un grafo. cada vertice es un
%partido, las aristas indican partidos que tienen un equipo en comun y por
%lo tanto no pueden jugarse al mismo tiempo. los primeros 6 vertices son
%los 6 partidos del grupo 1, los vertices del 7 al 12 son los partidos del
%grupo 2, etc. Los colores son los dias en los que se juegan los partidos,
%y las restricciones son: no mas de 4 partidos por dia, el primer dia el
%unico juego es el inaugural, y los partidos que tienen equipos en comun
%deben de tener al menos tres dias entre ellos. 
%el resultado es de 15 dias de partidos, exactamente como ocurrira en el
%mundial.
v=[1 2 3; 1 4 5; 2 4 6; 3 5 6];
mat=v;
for i=1:7
    mat=[mat; 6*i+1 6*i+2 6*i+3 ; 6*i+1 6*i+4 6*i+5; 2+6*i 4+6*i 6+6*i; 3+6*i 5+6*i 6+6*i];
end
sep=3;% separacion entre colores de vertices adyascentes ( al menos 3 dias entre partidos de un mismo equipo)
k=4; %max. vertices/color (partidos/dia)
restri=2*ones(48,1);
restri(1)=1;
s=main(mat,restri,k,sep);